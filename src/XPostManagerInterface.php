<?php

namespace Drupal\social_post_x;

use Abraham\TwitterOAuth\TwitterOAuth as XOAuth;

/**
 * Defines an interface for the X Post Manager.
 */
interface XPostManagerInterface {

  /**
   * Sets the token.
   *
   * @param string $oauth_token
   *   The oauth token.
   * @param string $oauth_token_secret
   *   The oauth token secret.
   */
  public function setOauthToken($oauth_token, $oauth_token_secret);

  /**
   * Sets the X client.
   *
   * @param \Abraham\TwitterOAuth\XOAuth $client
   *   The API client.
   */
  public function setClient(XOAuth $client);

  /**
   * Wrapper for post method.
   *
   * @param string|array $tweet
   *   The tweet text (with optional media paths).
   */
  public function doPost($tweet);

  /**
   * Uploads files by path.
   *
   * @param array $paths
   *   The paths for media to upload.
   */
  public function uploadMedia(array $paths);

}
