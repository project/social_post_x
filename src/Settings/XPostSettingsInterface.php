<?php

namespace Drupal\social_post_x\Settings;

/**
 * Defines an interface for Social Post X settings.
 */
interface XPostSettingsInterface {

  /**
   * Gets the consumer key.
   *
   * @return string
   *   The consumer key.
   */
  public function getConsumerKey();

  /**
   * Gets the consumer secret.
   *
   * @return string
   *   The consumer secret.
   */
  public function getConsumerSecret();

  /**
   * Gets the OAuth token.
   *
   * @return string
   *   The OAuth token.
   */
  public function getOpenAuthToken();

  /**
   * Gets the OAuth token secret.
   *
   * @return string
   *   The OAuth token secret.
   */
  public function getOpenAuthTokenSecret();

}
