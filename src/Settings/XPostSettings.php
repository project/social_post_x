<?php

namespace Drupal\social_post_x\Settings;

use Drupal\social_api\Settings\SettingsBase;

/**
 * Returns the app information.
 */
class XPostSettings extends SettingsBase implements XPostSettingsInterface {

  /**
   * Consumer key.
   *
   * @var string
   */
  protected $consumerKey;

  /**
   * Consumer secret.
   *
   * @var string
   */
  protected $consumerSecret;

  /**
   * OAuth token.
   *
   * @var string
   */
  protected $openAuthToken;

  /**
   * OAuth token secret.
   *
   * @var string
   */
  protected $openAuthTokenSecret;

  /**
   * {@inheritdoc}
   */
  public function getConsumerKey() {
    if (!$this->consumerKey) {
      $this->consumerKey = $this->config->get('consumer_key');
    }

    return $this->consumerKey;
  }

  /**
   * {@inheritdoc}
   */
  public function getConsumerSecret() {
    if (!$this->consumerSecret) {
      $this->consumerSecret = $this->config->get('consumer_secret');
    }

    return $this->consumerSecret;
  }

  /**
   * {@inheritdoc}
   */
  public function getOpenAuthToken() {
    if (!$this->openAuthToken) {
      $this->openAuthToken = $this->config->get('oauth_token');
    }

    return $this->openAuthToken;
  }

  /**
   * {@inheritdoc}
   */
  public function getOpenAuthTokenSecret() {
    if (!$this->openAuthTokenSecret) {
      $this->openAuthTokenSecret = $this->config->get('oauth_token_secret');
    }

    return $this->openAuthTokenSecret;
  }

}
