<?php

namespace Drupal\social_post_x;

use Abraham\TwitterOAuth\TwitterOAuth as XOAuth;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\social_api\SocialApiException;

/**
 * Manages the authorization process before getting a long lived access token.
 */
class XPostManager implements XPostManagerInterface {
  use LoggerChannelTrait;

  /**
   * The X client.
   *
   * @var \Abraham\TwitterOAuth\XOAuth
   */
  protected $client;

  /**
   * The tweet text (with optional media ids).
   *
   * @var array
   */
  protected $tweet;

  /**
   * {@inheritdoc}
   */
  public function setClient(XOAuth $client) {
    $this->client = $client;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOauthToken($oauth_token, $oauth_token_secret) {
    $this->client->setOauthToken($oauth_token, $oauth_token_secret);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function doPost($tweet) {

    // Make backwards-compatible if someone just posts a tweet text as a string.
    $this->tweet['text'] = is_array($tweet) && !empty($tweet['text']) ? $tweet['text'] : $tweet;

    // Check if there needs to be media uploaded. If so, upload and store ids.
    if (!empty($tweet['media_paths'])) {
      $this->tweet['media']['media_ids'] = $this->uploadMedia($tweet['media_paths']);
    }

    try {
      return $this->post();
    }
    catch (SocialApiException $e) {
      $this->getLogger('social_post_x')->error($e->getMessage());

      return FALSE;
    }

  }

  /**
   * {@inheritdoc}
   */
  public function uploadMedia(array $paths) {
    $media_ids = [];
    $chunked = [];
    $this->client->setApiVersion(1.1);

    foreach ($paths as $path) {
      // Upload the media from the path.
      try {
        $media = $this->client->upload('media/upload', ['media' => $path], $chunked);
      }
      catch (SocialApiException $e) {
        $this->getLogger('social_post_x')->error($e->getMessage());
        continue;
      }

      $chunked = ['chunkedUpload' => TRUE];
      // The response contains the media_ids to attach the media to the post.
      if ($media) {
        $media_ids[] = $media->media_id_string;
      }
    }

    $this->client->setApiVersion(2);

    return $media_ids;
  }

  /**
   * Post the tweet with the client.
   *
   * @return bool
   *   TRUE on success, FALSE otherwise (with a logger message).
   */
  protected function post() {

    $get = $this->client->get("account/verify_credentials");

    $post = $this->client->post("tweets", $this->tweet, ['jsonPayload' => TRUE]);

    if (isset($get->errors)) {
      $this->getLogger('social_post_x')->error($get->errors[0]->message);

      return FALSE;
    }
    if (isset($post->errors) || isset($get->errors)) {
      $this->getLogger('social_post_x')->error($post->errors[0]->message);

      return FALSE;
    }

    return TRUE;
  }

}
