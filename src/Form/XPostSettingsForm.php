<?php

namespace Drupal\social_post_x\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Settings form for Social Post X.
 */
class XPostSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['social_post_x.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'social_post_x.form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('social_post_x.settings');

    $form['x_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('X settings'),
      '#open' => TRUE,
    ];

    $form['x_settings']['consumer_key'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Consumer Key (API Key)'),
      '#default_value' => $config->get('consumer_key'),
      '#description' => $this->t('Copy the Consumer Key here'),
    ];

    $form['x_settings']['consumer_secret'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Consumer Secret (API Secret)'),
      '#default_value' => $config->get('consumer_secret'),
      '#description' => $this->t('Copy the Consumer Secret here'),
    ];

    $form['x_settings']['oauth_token'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('OAuth Token (Access Token)'),
      '#default_value' => $config->get('oauth_token'),
      '#description' => $this->t('Copy the OAuth Token here'),
    ];

    $form['x_settings']['oauth_token_secret'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('OAuth Token Secret (Access Token Secret)'),
      '#default_value' => $config->get('oauth_token_secret'),
      '#description' => $this->t('Copy the OAuth Token Secret here'),
    ];

    $form['x_settings']['oauth2_client_id'] = [
      '#type' => 'textfield',
      '#required' => FALSE,
      '#title' => $this->t('OAuth2 Client Id'),
      '#default_value' => $config->get('oauth2_client_id'),
      '#description' => $this->t('Copy the OAuth2 Client ID here'),
    ];

    $form['x_settings']['oauth2_client_secret'] = [
      '#type' => 'textfield',
      '#required' => FALSE,
      '#title' => $this->t('OAuth2 Client Secret'),
      '#default_value' => $config->get('oauth2_client_secret'),
      '#description' => $this->t('Copy the OAuth2 Client Secret here'),
    ];

    $form['x_settings']['callback_urls'] = [
      '#type' => 'textfield',
      '#disabled' => TRUE,
      '#title' => $this->t('Callback URL'),
      '#description' => $this->t('Copy this value to <em>Callback URLs</em> field of your X App settings.'),
      '#default_value' => $GLOBALS['base_url'] . '/social-post-x/x/auth/callback',
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    $this->config('social_post_x.settings')
      ->set('consumer_key', $values['consumer_key'])
      ->set('consumer_secret', $values['consumer_secret'])
      ->set('oauth_token', $values['oauth_token'])
      ->set('oauth_token_secret', $values['oauth_token_secret'])
      ->set('oauth2_client_id', $values['oauth2_client_id'])
      ->set('oauth2_client_secret', $values['oauth2_client_secret'])
      ->save();

    parent::submitForm($form, $form_state);
  }

}
