<?php

namespace Drupal\social_post_x\Plugin\Network;

use Drupal\social_post\Plugin\Network\NetworkInterface;

/**
 * Defines an interface for X Post Network Plugin.
 */
interface XPostInterface extends NetworkInterface {

  /**
   * Gets the absolute url of the callback.
   *
   * @return string
   *   The callback url.
   */
  public function getOauthCallback();

}
