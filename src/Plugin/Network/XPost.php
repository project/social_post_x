<?php

namespace Drupal\social_post_x\Plugin\Network;

use Abraham\TwitterOAuth\TwitterOAuth as XOAuth;
use Drupal\Core\Url;
use Drupal\social_api\SocialApiException;
use Drupal\social_post\Plugin\Network\NetworkBase;

/**
 * Defines Social Post X Network Plugin.
 *
 * @Network(
 *   id = "social_post_x",
 *   social_network = "X",
 *   type = "social_post",
 *   handlers = {
 *     "settings": {
 *        "class": "\Drupal\social_post_x\Settings\XPostSettings",
 *        "config_id": "social_post_x.settings"
 *      }
 *   }
 * )
 */
class XPost extends NetworkBase implements XPostInterface {

  /**
   * {@inheritdoc}
   */
  protected function initSdk() {
    $class_name = '\Abraham\TwitterOAuth\TwitterOAuth';
    if (!class_exists($class_name)) {
      throw new SocialApiException(sprintf('The PHP SDK for X could not be found. Class: %s.', $class_name));
    }

    /** @var \Drupal\social_post_x\Settings\XPostSettings $settings */
    $settings = $this->settings;

    return new XOAuth($settings->getConsumerKey(), $settings->getConsumerSecret(), $settings->getOpenAuthToken(), $settings->getOpenAuthTokenSecret());
  }

  /**
   * {@inheritdoc}
   */
  public function getOauthCallback() {
    return Url::fromRoute('social_post_x.callback')->setAbsolute()->toString();
  }

}
