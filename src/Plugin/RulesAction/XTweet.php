<?php

namespace Drupal\social_post_x\Plugin\RulesAction;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\rules\Core\RulesActionBase;
use Drupal\social_api\User\UserManagerInterface;
use Drupal\social_post_x\Plugin\Network\XPostInterface;
use Drupal\social_post_x\XPostManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'Tweet' action.
 *
 * @RulesAction(
 *   id = "social_post_x_tweet",
 *   label = @Translation("X Tweet"),
 *   category = @Translation("Social Post"),
 *   context_definitions = {
 *     "status" = @ContextDefinition("string",
 *       label = @Translation("X Tweet content"),
 *       description = @Translation("Specifies the status to post.")
 *     )
 *   }
 * )
 */
class XTweet extends RulesActionBase implements ContainerFactoryPluginInterface {

  /**
   * The X post network plugin.
   *
   * @var \Drupal\social_post_x\Plugin\Network\XPostInterface
   */
  protected $xPoster;

  /**
   * The social api user manager.
   *
   * @var \Drupal\social_post\User\UserManager
   */
  protected $userManager;

  /**
   * The X post network plugin.
   *
   * @var \Drupal\social_post_x\XPostManager
   */
  protected $postManager;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    /** @var \Drupal\social_post_x\Plugin\Network\XPost $x_post*/
    $x_poster = $container->get('plugin.network.manager')->createInstance('social_post_x');

    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $x_poster,
      $container->get('social_post.user_manager'),
      $container->get('x_post.manager'),
      $container->get('current_user')
    );
  }

  /**
   * Tweet constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\social_post_x\Plugin\Network\XPostInterface $x_post
   *   The X post network plugin.
   * @param \Drupal\social_api\User\UserManagerInterface $user_manager
   *   The social user manager.
   * @param \Drupal\social_post_x\XPostManagerInterface $post_manager
   *   The X post manager.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   */
  public function __construct(array $configuration,
                              $plugin_id,
                              $plugin_definition,
                              XPostInterface $x_post,
                              UserManagerInterface $user_manager,
                              XPostManagerInterface $post_manager,
                              AccountInterface $current_user) {

    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->xPoster = $x_post;
    $this->userManager = $user_manager;
    $this->postManager = $post_manager;
    $this->currentUser = $current_user;

    $client = $this->xPoster->getSdk();
    $this->postManager->setClient($client);
  }

  /**
   * {@inheritdoc}
   */
  public function execute() {
    $text = $this->getContextValue('status');
    $this->postManager->doPost(['text' => $text]);
  }

}
