Introduction
------------

 * Social Post X allows you to configure your site to automatically tweet
   to a user account without human intervention.

 * Social Post X integrates with the Rules module (recommended).

Requirements
------------

This module requires the following modules:
 * Social Post (https://www.drupal.org/project/social_post)

Installation
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://www.drupal.org/node/1897420 for further information.

Rules based usage
-------------

First install and configure this module from:
/admin/config/social-api/social-post/x

Here you will need to enter the following credentials:

- X Consumer Key (API Key)
- X Consumer Secret (API Secret)
- X OAuth Token (Access Token)
- X OAuth Token Secret (Access Token Secret)

Navigate to Configuration > Rules (/admin/config/workflow/rules) and click 
"Add reaction rule". Select a React on event value (e.g. "After saving a new 
content node"). Set a Label for the rule (e.g. "New content node tweet").
Click "Save".
On the rule edit page, click "Add action".
Select the "X Tweet" action under the "Social Post" option group in the Action
field.
Click "Continue".
Input the text to be tweeted -- e.g. A new article "{{ node.title.value }}" has
been published on my website at {{ node | entity_url }}
Click "Save".
Add other conditions and/or actions as desired.
Click "Save".

Example use case:

 * Post a tweet when new content is created.

Maintainers
-----------
Current maintainers:

 * Daniel Lobo - https://www.drupal.org/u/2dareis2do
