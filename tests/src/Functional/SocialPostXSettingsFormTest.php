<?php

namespace Drupal\Tests\social_post_x\Functional;

use Drupal\Tests\social_post\Functional\SocialPostTestBase;

/**
 * Test Social Post X settings form.
 *
 * @group social_post
 *
 * @ingroup social_post_x
 */
class SocialPostXSettingsFormTest extends SocialPostTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['social_post_x'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    $this->module = 'social_post_x';
    $this->provider = 'X';
    parent::setUp();
  }

  /**
   * Test if implementer is shown in the integration list.
   */
  public function testIsAvailableInIntegrationList() {
    $this->fields = ['consumer_key', 'consumer_secret'];

    $this->checkIsAvailableInIntegrationList();
  }

  /**
   * Test if permissions are set correctly for settings page.
   *
   * @throws \Behat\Mink\Exception\ElementNotFoundException
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testPermissionForSettingsPage() {
    $this->checkPermissionForSettingsPage();
  }

  /**
   * Test settings form submission.
   */
  public function testSettingsFormSubmission() {
    $this->edit = [
      'consumer_key' => $this->randomString(10),
      'consumer_secret' => $this->randomString(10),
    ];

    $this->checkSettingsFormSubmission();
  }

}
